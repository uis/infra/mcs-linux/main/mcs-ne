/*************************************************
*       The E text editor - 3rd incarnation      *
*************************************************/

/* Copyright (c) University of Cambridge, 1991 - 2012 */

/* Written by Philip Hazel, starting November 1991 */

/* This file contains one function, which sets up the current version and 
copyright strings. */

#define VERSION    "3.02"
#define COPYRIGHT  "Copyright (c) University of Cambridge 2012"

#include "ehdr.h"

void version_init(void)
{
int i = 0;
uschar today[20];

version_copyright = US COPYRIGHT;
version_string = US VERSION;

Ustrcpy(today, __DATE__);
if (today[4] == ' ') i = 1;
today[3] = today[6] = '-';

version_date = US malloc(20);
Ustrcpy(version_date, "(");
Ustrncat(version_date, today+4+i, 3-i);
Ustrncat(version_date, today, 4);
Ustrncat(version_date, today+7, 4);
Ustrcat(version_date, ")");
}

/* End of c.eversion */
